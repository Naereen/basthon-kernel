all:

FILE_PACKAGER = extern/emscripten/tools/file_packager.py
BASTHON_DATA = src/basthon-py/build/basthon.data
BASTHON_JS = src/basthon-py/build/basthon.js
BASTHON_MODULE = src/basthon-py/basthon
BASTHON_VFS_DEST = /lib/python3.8/site-packages/basthon/

MINIFIED=src/basthon-js/basthon.min.js src/basthon-js/basthon_goodies.min.js
minify: $(MINIFIED)

# this is a workaround for the buggy sources= parameter in map file
# produced by uglifyjs.terser
%.min.js: %.js
	cd $(dir $<) && uglifyjs.terser $(notdir $<) --source-map url=$(notdir $@).map --compress --mangle --output $(notdir $@) && cd -

extern/emscripten:
	git clone --depth 1 https://github.com/emscripten-core/emsdk.git
	cd emsdk/ && ./emsdk install --build=Release 1.38.44-fastcomp && cd -
	cat extern/patches/emsdk_*.patch | patch -p1
	mv emsdk/fastcomp/emscripten/ extern/
	rm -rf emsdk/

setup-emscripten: extern/emscripten

$(BASTHON_DATA): $(BASTHON_JS)

$(BASTHON_JS): setup-emscripten
	python3 $(FILE_PACKAGER)  # this ensure ~/.emscripten is created
	mkdir -p $(dir $@)
	python3 $(FILE_PACKAGER) $(BASTHON_DATA) \
		--js-output=$(BASTHON_JS) \
	 	--preload $(BASTHON_MODULE)"@"$(BASTHON_VFS_DEST) \
	 	--lz4 \
	 	--use-preload-plugins \
	 	--export-name="pyodide._module" \
	 	--exclude "*__pycache__*"

wasm-package: $(BASTHON_DATA)

build: minify wasm-package
	cd setup/ && make build

publish:
	cd setup/ && make publish

clean:
	cd setup/ && make clean
	rm -rf $(MINIFIED) $(BASTHON_DATA) $(BASTHON_JS) tests/build/

install:
	pip3 install --force-reinstall setup/dist/basthon-kernel-*.tar.gz

www:
	cd www/ && make html && cd -

www-publish: www
	rsync -avzP --delete www/output/ basthon:sites_basthon/root/

devel-www-publish: www
	find www/output/ \( -type d -name .git -prune \) -o -type f -print0 | xargs -0 sed -i 's/https:\/\/console\.basthon\.fr/https:\/\/devel\.basthon\.fr\/console/g'
	find www/output/ \( -type d -name .git -prune \) -o -type f -print0 | xargs -0 sed -i 's/https:\/\/notebook\.basthon\.fr/https:\/\/devel\.basthon\.fr\/notebook/g'
	rsync -avzP --delete www/output/ --exclude=console/ --exclude=notebook/ --exclude=.htaccess basthon:sites_basthon/devel/

test-www: www
	bash -c "set -m ; python3 -m http.server --directory www/output/ --bind localhost 8888 & sleep 1 ; firefox localhost:8888 ; fg %1"

test: build install
	mkdir -p tests/build/
	cd tests/build/ && python3 -m basthon-kernel --install && cd -
	cp tests/test.html tests/build/
	pytest --junitxml=test_report.xml tests/

.PHONY: all build setup-emscripten wasm-package publish clean install www www-publish test test-www minify devel-www-publish
