#!/usr/bin/env python3
import urllib
from pathlib import Path

AUTHOR = 'Romain Casati'
SITENAME = 'Basthon'
SITEURL = ''

PATH = 'content'

TIMEZONE = 'Europe/Paris'

DEFAULT_LANG = 'fr'

THEME = 'theme'

DIRECT_TEMPLATES = ('index', 'about', 'galerie', 'doc', 'contrib')

STATIC_PATHS = [
    '.htaccess',
]

EXTRA_PATH_METADATA = {
    '.htaccess': {'path': '.htaccess'},
}

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = []

# Social widget
SOCIAL = []

DEFAULT_PAGINATION = False

# Uncomment following line if you want document-relative URLs when developing
RELATIVE_URLS = True


def readme(*args):
    import markdown
    with open("../README.md") as f:
        return markdown.markdown(f.read(), extensions=['attr_list', 'tables'])


def galerie(*args):
    from galerie import examples
    SITEURL = args[0]
    one_img = """
<div class="gallery-item">
  <div style="position: relative; top: 0; left: 0;">
    <a target="_blank" href="{SITEURL}/theme/assets/img/galerie/{img_name}" style="position: relative; top: 0; left: 0; cursor: zoom-in;">
      <img src="{SITEURL}/theme/assets/img/galerie/thumbnails/{img_name}" title="Voir l'image">
    </a>
    <a target="_blank" class="shadowed" href="{url}" style="position: absolute; top: calc(50% - 25px); left: calc(50% - 25px);">
      <img src="{SITEURL}/theme/assets/img/play.svg" style="width: 50px;" title="Voir le code dans Basthon" class="python-yellow"/>
    </a>
  </div>
  <div class="desc">{description}</div>
</div>"""

    def url(file, aux):
        url = str(Path("examples") / file)
        domain = 'console' if file.endswith('.py') else 'notebook'
        url = f"https://{domain}.basthon.fr/?from={urllib.parse.quote(url)}"
        if aux is not None:
            url += f"&aux={urllib.parse.quote(aux)}"
        return url

    return "".join(one_img.format(SITEURL=SITEURL,
                                  img_name=f"{Path(file).with_suffix('')}.png",
                                  description=data['desc'],
                                  url=url(file, data.get('aux')))
                   for file, data in examples.items())


JINJA_FILTERS = {"readme": readme,
                 "galerie": galerie}
