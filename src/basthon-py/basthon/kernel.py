"""
This is the Python part of the Basthon Kernel.
"""
import os
import sys
import importlib
from js import window
from ._console import InteractiveConsole
from . import packages


__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"

# where we put user supplied modules
_user_modules_root = "/basthon_user_modules"
# we don't insert at position 0 since
# https://github.com/iodide-project/pyodide/issues/737#issuecomment-750858417
sys.path.insert(2, _user_modules_root)
# can't figure out why this is needed...
importlib.invalidate_caches()


# interpretation system is the Pyodide's interactive console
_console = InteractiveConsole()


def locals():
    """ Global evaluation namespace. """
    return _console.locals


def display_event(data):
    """ Dispatching eval.display event with display data. """
    display_data = {}
    # Updating display data with evaluation data.
    # get evaluation data from namespace
    eval_data = _console.locals['__eval_data__']
    if eval_data is not None:
        display_data.update(eval_data)
    display_data.update(data)
    window.Basthon.dispatchEvent("eval.display", display_data)


def format_repr(obj):
    """ Format data to support different repr types. """
    res = {"text/plain": repr(obj)}
    if hasattr(obj, "_repr_html_"):
        res["text/html"] = obj._repr_html_()
    if hasattr(obj, "_repr_svg_"):
        res["image/svg+xml"] = obj._repr_svg_()
    if hasattr(obj, "_repr_png_"):
        res["image/png"] = obj._repr_png_()
    return res


def display(obj):
    """ Emulating the IPython.core.display.display function """
    display_event({'display_type': 'multiple',
                   'content': format_repr(obj)})


def download(filename):
    """
    Download a file from the local filesystem via a browser dialog.
    """
    return get_file(filename)


def put_file(filepath, content):
    """
    Put a file on the (emulated) local filesystem.
    """
    dirname, _ = os.path.split(filepath)
    if dirname:
        os.makedirs(dirname, exist_ok=True)

    with open(filepath, 'wb') as f:
        f.write(content)


def put_module(filename, content):
    """
    Put a module (*.py file) on the (emulated) local filesystem
    bypassing the Pyodide' single-import-issue by invalidate caches
    https://github.com/iodide-project/pyodide/issues/737

    /!\ Warning: the dependencies loading is done on the JS side by
    basthon.js.
    """
    def callback(*args):
        _, fname = os.path.split(filename)
        module_path = os.path.join(_user_modules_root, fname)
        put_file(module_path, content)
        file_finder = sys.path_importer_cache.get(_user_modules_root)
        if file_finder is None:
            # can't figure out why this is needed...
            importlib.invalidate_caches()
        else:
            file_finder.invalidate_caches()

    pkgs = packages.find_imports(content)
    return packages.load_and_hack(pkgs).then(callback)


def get_file(filepath):
    """
    Download a file from the (emulated) local filesystem.
    """
    _, filename = os.path.split(filepath)
    with open(filepath, 'rb') as f:
        window.Basthon.download(f.read(), filename)


def importables():
    """ List of all importable modules. """
    import sys
    import pkgutil
    from . import packages
    from_sys = set(x for x in sys.modules.keys() if '.' not in x)
    from_pkgutil = set(p.name for p in pkgutil.iter_modules())
    from_basthon = packages._all_pkgs
    return sorted(from_sys.union(from_pkgutil, from_basthon))


def restart():
    return _console.restart()


def execution_count():
    return _console.execution_count


# copying methods from _console to this module
for f in ('eval', 'complete', 'banner', 'more'):
    globals()[f] = getattr(_console, f)
