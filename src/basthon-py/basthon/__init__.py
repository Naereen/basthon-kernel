from . import kernel, _hack_builtins


__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"


__all__ = ['display', 'download']


display = kernel.display
download = kernel.download


_hack_builtins.hack_input()
_hack_builtins.hack_help()
