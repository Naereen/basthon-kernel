import pydoc
import builtins
import sys
from js import window
from . import packages


__author__ = "Romain Casati"
__license__ = "GNU GPL v3"
__email__ = "romain.casati@basthon.fr"


__all__ = ['hack_input', 'hack_help']


def hack_input():
    """ Hacking Pyodide bugy input function. """
    _default_input = builtins.input

    def _hacked_input(prompt=None):
        if prompt is not None:
            print(prompt, end='', flush=True)
        res = window.prompt(prompt)
        print(res)
        return res

    # copying all writable attributes (usefull to keep docstring and name)
    for a in dir(_default_input):
        try:
            setattr(_hacked_input, a, getattr(_default_input, a))
        except Exception:
            pass

    # replacing
    builtins.input = _hacked_input


def hack_help():
    """ Hacking help function.

    See pydoc.py in cpython:
    https://github.com/python/cpython/blob/master/Lib/pydoc.py
    it uses a class called ModuleScanner to list packages.
    this class first looks at sys.builtin_module_names then in pkgutil.
    we fake sys.builtin_module_names in order to get it right
    """
    _default_help = pydoc.help

    def _hacked_help(*args, **kwargs):
        backup = sys.builtin_module_names
        to_add = list(packages._all_pkgs)
        to_add.append('js')
        sys.builtin_module_names = backup + tuple(to_add)
        res = _default_help(*args, **kwargs)
        sys.builtin_module_names = backup
        return res

    pydoc.help = _hacked_help
