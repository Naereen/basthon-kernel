import basthon

__all__ = ["display", "display_image", "HTML"]


display = basthon.display


def display_image(img):
    """ Displaying image from numpy array  """
    from matplotlib import image
    import base64
    import io

    def _repr_png_():
        raw = io.BytesIO()
        image.imsave(raw, img, format="png", cmap='gray')
        raw.seek(0)
        return base64.b64encode(raw.read()).decode()

    dummy = type('image', (), {})()
    dummy._repr_png_ = _repr_png_
    display(dummy)


def HTML(html):
    """ Sending html string to IPython notebook. """
    def _repr_html_():
        return html
    dummy = type('HTML', (), {})()
    dummy._repr_html_ = _repr_html_
    return display(dummy)
