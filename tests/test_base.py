from pathlib import Path
import json


def test_setup(selenium):
    assert selenium.run_js("return pyodide.version()") == "0.16.1"
    assert selenium.run_js("return Basthon.pythonVersion") == "3.8.2"
    assert selenium.run("1 + 1") == 2


def test_result(selenium):
    data = selenium.run_basthon("1 + 1")['result']
    assert data['result']['text/plain'] == '2' and data['execution_count'] == 1


def test_streams(selenium):
    data = selenium.run_basthon("print('foo')")
    assert data['stdout'] == "foo\n" and data['stderr'] == ""
    data = selenium.run_basthon("import sys ; print('bar', file=sys.stderr)")
    assert data['stdout'] == "" and data['stderr'] == "bar\n"


def test_errors(selenium):
    data = selenium.run_basthon("1:")
    assert data['stderr'] == '  File "<input>", line 1\n    1:\n     ^\nSyntaxError: invalid syntax\n'
    assert 'result' not in data['result'] and data['stdout'] == ""
    data = selenium.run_basthon("1 / 0")
    assert data['stderr'] == 'Traceback (most recent call last):\n  File "<input>", line 1, in <module>\nZeroDivisionError: division by zero\n'
    assert 'result' not in data['result'] and data['stdout'] == ""


def test_put_file(selenium):
    selenium.run_js("""
    window.toBytesArray = function(string) {
        string = unescape(encodeURIComponent(string));
        const arr = [];
        for (var i = 0; i < string.length; i++) {
            arr.push(string.charCodeAt(i));
        }
        return Uint8ClampedArray.from(arr);
    }""")

    content = 'hello\n world! ¥£€$¢₡₢₣₤₥₦₧₨₩₪₫₭₮₯₹'
    selenium.driver.execute_script("Basthon.putFile('foo.txt', toBytesArray(arguments[0]))", content)
    data = selenium.run_basthon("""
    with open('foo.txt') as f:
        print(f.read(), end='', flush=True)""")['stdout']
    assert data == content


def test_put_module(selenium):
    content = "foo = 42"
    selenium.driver.execute_async_script("""
    const done = arguments[arguments.length - 1];
    const content = toBytesArray(arguments[0]);
    Basthon.putModule('bar.py', content).then(done);""", content)
    result = selenium.run_basthon("""
    import bar
    print(bar.foo, end='', flush=True)""")['stdout']
    assert result == "42"

    # with a second module

    content = "bar = 24"
    selenium.driver.execute_async_script("""
    const done = arguments[arguments.length - 1];
    const content = toBytesArray(arguments[0]);
    Basthon.putModule('foo.py', content).then(done);""", content)
    result = selenium.run_basthon("""
    import foo
    foo.bar""")['result']['result']['text/plain']
    assert result == "24"


def test_importables(selenium):
    result = selenium.run_basthon("from basthon import kernel ; kernel.importables()")['result']
    importables = eval(result['result']['text/plain'])
    with open(Path(__file__).parent / "data" / "importables.json") as f:
        target = json.load(f)
    assert importables == target


def test_import_not_hacked(selenium):
    data = selenium.run_basthon("import numpy as np; np.pi")
    assert data['result']['result']['text/plain'] == "3.141592653589793"
    # 0.16.1 shows numpy's warnings (Python 3.8)
    if selenium.run_js("return pyodide.version()") != "0.16.1":
        assert data['stderr'] == ""


def test_banner(selenium):
    banner = selenium.run_js("return Basthon.banner();")
    assert banner.startswith("Python 3.")
    assert 'Type "help", "copyright", "credits" or "license" for more information' in banner


def test_more(selenium):
    tests = [
        ("for i in range(10)", False),
        ("for i in range(10):", True),
        ("for i in range(10):\\n    print(i)", False),
        ("def f(x)", False),
        ("def f(x):", True),
        ("def f(x):\\n    return 2 * x + 1", False),
    ]
    for t, v in tests:
        assert selenium.run_js(f"""return Basthon.more("{t}");""") == v
