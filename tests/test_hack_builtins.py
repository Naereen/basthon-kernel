from pathlib import Path
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions


def test_hack_input(selenium):
    selenium.run_basthon_detach("int(input('How old are you?'))")
    driver = selenium.driver
    wait = WebDriverWait(driver, 10)
    wait.until(expected_conditions.alert_is_present())
    alert = driver.switch_to.alert
    alert.send_keys("42")
    alert.accept()
    result = selenium.run_basthon_reattach()['result']['result']['text/plain']
    assert result == "42"


def test_hack_modules(selenium):
    data = selenium.run_basthon("help('modules')")
    assert 'result' not in data['result'] and data['stderr'] == ""
    with open(Path(__file__).parent / 'data' / 'modules.txt') as f:
        assert data['stdout'] == f.read()
