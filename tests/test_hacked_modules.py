from pathlib import Path
import re

_test_data = Path(__file__).parent / "data"


def test_all(selenium):
    # ensure all hacked modules are tested
    tested = set(g[len('test_'):] for g in globals()
                 if g.startswith('test_') and g != 'test_all')
    result = selenium.run_basthon("""
    from basthon import _hack_modules
    set(g[len('hack_'):] for g in dir(_hack_modules)
        if g.startswith('hack_'))""")['result']
    hacked = eval(result['result']['text/plain'])
    assert tested == hacked


def test_PIL(selenium):
    result = selenium.run_basthon("""
    from PIL import Image, ImageDraw
    w, h = 120, 90
    bbox = [(10, 10), (w - 10, h - 10)]

    img = Image.new("RGB", (w, h), "#f9f9f9")
    dctx = ImageDraw.Draw(img)
    dctx.rectangle(bbox, fill="#ddddff", outline="blue")
    img.show()
    """)
    assert result['stderr'] == ""
    assert result['display']['display_type'] == 'multiple'
    img = result['display']['content']['image/png']
    with open(_test_data / "pil.png") as f:
        target = f.read()
    assert target == img


def test_matplotlib(selenium):
    selenium.run_basthon("""
    import matplotlib.pyplot as plt

    plt.figure()
    plt.plot([0, 1], [0, 1])
    plt.show()
    """, return_data=False)
    # can't access content like this
    # elem = result['content']
    # because of selenium's StaleElementReferenceException
    # bypassing it via JS
    html = selenium.run_js("return window._basthon_eval_data.display.content.outerHTML")
    html = re.sub('matplotlib_[0-9a-f]+', 'matplotlib_', html)
    with open(_test_data / 'matplotlib.html') as f:
        target = f.read()
    assert html == target


def test_folium(selenium):
    result = selenium.run_basthon("""
    import folium

    m = folium.Map(location=[47.228382, 2.062796],
                   zoom_start=17)

    m.display()
    """)
    assert result['stderr'] == ""
    assert result['display']['display_type'] == 'html'
    map = re.sub('map_[0-9a-f]+', 'map_', result['display']['content'])
    map = re.sub('tile_layer_[0-9a-f]+', 'tile_layer_', map)
    with open(_test_data / "folium.html") as f:
        target = f.read()
    assert target == map


def test_pandas(selenium):
    result = selenium.run_basthon("""
    import pandas as pd

    df = pd.DataFrame({
        'language': ["Python", "C", "Java"],
        'verbosity': [0.1, 0.5, 0.9]
    })

    df.display()
    """)
    assert result['stderr'] == ""
    assert result['display']['display_type'] == 'html'
    with open(_test_data / "pandas.html") as f:
        assert result['display']['content'] == f.read()


def test_sympy(selenium):
    result = selenium.run_basthon("""
    import sympy
    from sympy.abc import x
    sympy.pretty_print(sympy.sqrt(x ** 2 + sympy.pi))
    """)
    assert result['stderr'] == ""
    assert result['display']['display_type'] == 'sympy'
    assert result['display']['content'] == "$$\\sqrt{x^{2} + \\pi}$$"


def test_turtle(selenium):
    selenium.run_basthon("""
    import turtle
    turtle.forward(100)
    turtle.done()
    """, return_data=False)
    # can't access content like this
    # elem = result['content']
    # because of selenium's StaleElementReferenceException
    # bypassing it via JS
    svg = selenium.run_js("return window._basthon_eval_data.display.content.outerHTML")
    svg = re.sub('\"af_[0-9a-f]+_', '\"af_', svg)
    with open(_test_data / 'turtle.svg') as f:
        target = f.read()
    assert svg == target
